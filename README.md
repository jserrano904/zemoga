# Zemoga

### Instructions

1. Just download the project and run  it.

### Achitecture

This project implemments  clean architucture using use cases, interactors, presenters, viewControllers and Cells with a specific resposabilities for each layer.

### Core Data

This project uses Core Data as persistence framework. Actually this project roght now has a small issue for updating the read and unread state of the post. Please keep it in mind, maybe with more time I would solve this issue.

### Screenshots

#### Persistence
![Scheme](ZemogaEvidence1.png)

#### App
![Scheme](ZemogaEvidence2.png)
![Scheme](ZemogaEvidence3.png)
![Scheme](ZemogaEvidence4.png)
![Scheme](ZemogaEvidence5.png)
![Scheme](ZemogaEvidence6.png)


