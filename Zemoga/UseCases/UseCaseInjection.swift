//
//  UseCaseInjection.swift
//  Zemoga
//
//  Created by jonathand alberto serrano serrano on 3/10/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

final class UseCaseLocator {
    static let shared: UseCaseLocator = UseCaseLocator()
    
    var getPosts: GetPosts {
        return GetPostsAdapter(dependencies: GetPostsAdapter.Dependencies())
    }
    
    var removeAllPost: DeleteAllPosts {
        return DeleteAllPostAdapter(dependencies: DeleteAllPostAdapter.Dependencies())
    }
    
    var getUser: GetUser {
        return GetUserAdapter(dependencies: GetUserAdapter.Dependencies())
    }
    
    var getComments: GetComments {
        return GetCommentsAdapter(dependencies: GetCommentsAdapter.Dependencies())
    }
    
    var updatePost: UpdatePost {
        return UpdatePostAdapter(dependencies: UpdatePostAdapter.Dependencies())
    }
    
    var getFavorites: GetFavorites {
        return GetFavoritesAdapter(dependencies: GetFavoritesAdapter.Dependencies())
    }
    
    var deletePost: DeletePost {
        return DeletePostAdapter(dependencies: DeletePostAdapter.Dependencies())
    }
}

extension UseCaseLocator {
    static private var injector: UseCaseLocator {
        return UseCaseLocator.shared
    }
    
    static func inject() -> GetPosts {
        return injector.getPosts
    }
    
    static func inject() -> DeleteAllPosts {
        return injector.removeAllPost
    }
    
    static func inject() -> GetUser {
        return injector.getUser
    }
    
    static func inject() -> GetComments {
        return injector.getComments
    }
    
    static func inject() -> UpdatePost {
        return injector.updatePost
    }
    
    static func inject() -> GetFavorites {
        return injector.getFavorites
    }
    
    static func inject() -> DeletePost {
        return injector.deletePost
    }
}
