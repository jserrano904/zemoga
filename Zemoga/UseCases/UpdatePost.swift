//
//  UpdatePost.swift
//  Zemoga
//
//  Created by Jonathand Alberto Serrano Serrano on 3/13/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

protocol UpdatePost {
    func execute(postId: Int, isFavorite: Bool, status: PostState, completion: PersistencePostCompletion)
}
