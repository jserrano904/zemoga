//
//  GetPosts.swift
//  Zemoga
//
//  Created by Jonathand Alberto Serrano Serrano on 3/12/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

enum PostResponse {
    case success(response: [Post])
    case failure(error: Error)
}

typealias PostsCompletion = (PostResponse) -> Void

protocol GetPosts {
    func execute(usingCache: Bool, completion: @escaping PostsCompletion)
}
