//
//  GetPostsAdapter.swift
//  Zemoga
//
//  Created by Jonathand Alberto Serrano Serrano on 3/12/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

final class GetPostsAdapter: GetPosts {
    struct Constants {
        static let posts: String = "https://jsonplaceholder.typicode.com/posts"
    }
    
    struct Dependencies {
        var webService: WebService = ServiceLocator.inject()
        var persistence: Persistence = ServiceLocator.inject()
    }
    
    let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func execute(usingCache: Bool = false, completion: @escaping PostsCompletion) {
        if usingCache {
            let posts = dependencies.persistence.fetchPosts()
            guard !posts.isEmpty else {
                getPostsFromWebService(completion: completion)
                return
            }
            
            completion(.success(response: posts))
        } else {
            getPostsFromWebService(completion: completion)
        }
    }
}

private extension GetPostsAdapter {
    func getPostsFromWebService(completion: @escaping PostsCompletion) {
        dependencies.webService.consumeEndpoint(endPoint: Constants.posts) { [weak self] (serviceResponse) in
            let generalError = NSError()
            switch (serviceResponse) {
            case .success(let data):
                guard
                    let data = data,
                    let posts = try? JSONDecoder().decode([PostAdapter].self, from: data).sorted(by: { $0.id < $1.id })
                    else {
                        return completion(.failure(error: generalError))
                }
                self?.dependencies.persistence.savePosts(posts: posts)
                completion(.success(response: posts))
            case .failure(let error):
                completion(.failure(error: error))
            }
        }
    }
}

private struct PostAdapter: Decodable {
    var userId: Int
    var id: Int
    var title: String
    var body: String
}

extension PostAdapter: Post {
    var favorite: Bool {
        return false
    }
    var status: PostState {
        return .unread
    }
}

