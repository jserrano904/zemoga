//
//  GetCommentsAdapter.swift
//  Zemoga
//
//  Created by Jonathand Alberto Serrano Serrano on 3/12/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

final class GetCommentsAdapter: GetComments {
    
    struct Constants {
        static let commentsEndPoint: String = "https://jsonplaceholder.typicode.com/comments"
    }
    
    struct Dependencies {
        var webService: WebService = ServiceLocator.inject()
    }
    
    var dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func execute(postId: Int, completion: @escaping CommentsCompletion) {
        let generalError = NSError()
        dependencies.webService.consumeEndpoint(endPoint: Constants.commentsEndPoint + "?postId=\(postId)") { (serviceResponse) in
            switch(serviceResponse) {
            case .success(let commentsData):
                guard
                    let commentsData = commentsData,
                    let comments = try? JSONDecoder().decode([CommentAdapter].self, from: commentsData)
                else {
                    completion(.failure(error: generalError))
                    return
                }
                completion(.success(comments: comments))
            case .failure(let error):
                completion(.failure(error: error))
            }
        }
    }
}

private struct CommentAdapter: Comment, Decodable {
    var id: Int
    var name: String
    var email: String
    var body: String
}
