//
//  DeleteAllPostAdapter.swift
//  Zemoga
//
//  Created by Jonathand Alberto Serrano Serrano on 3/12/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

final class DeleteAllPostAdapter: DeleteAllPosts {
    struct Dependencies {
        var persistence: Persistence = ServiceLocator.inject()
    }
    
    var dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func execute(completion: PersistencePostCompletion) {
        dependencies.persistence.deleteAllPosts(completion: completion)
    }
}
