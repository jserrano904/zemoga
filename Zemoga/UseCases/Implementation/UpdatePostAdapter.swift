//
//  UpdatePostAdapter.swift
//  Zemoga
//
//  Created by Jonathand Alberto Serrano Serrano on 3/13/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

final class UpdatePostAdapter: UpdatePost {
    
    struct Dependencies {
        var persistence: Persistence = ServiceLocator.inject()
    }
    
    var dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func execute(postId: Int, isFavorite: Bool, status: PostState, completion: PersistencePostCompletion) {
        dependencies.persistence.updatePost(postId, isFavorite: isFavorite, status: status, completion: completion)
    }
}
