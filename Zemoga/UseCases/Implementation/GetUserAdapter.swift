//
//  GetUserAdapter.swift
//  Zemoga
//
//  Created by Jonathand Alberto Serrano Serrano on 3/12/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

final class GetUserAdapter: GetUser {
    
    struct Constants {
        static let userEndPoint: String = "https://jsonplaceholder.typicode.com/users"
    }
    
    struct Dependencies {
        var webService: WebService = ServiceLocator.inject()
    }
    
    var dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func execute(userId: Int, completion: @escaping UserCompletion) {
        let generalError = NSError()
        dependencies.webService.consumeEndpoint(endPoint: Constants.userEndPoint + "?id=\(userId)") { [weak self] (serviceResponse) in
            guard let strongSelf = self else {
                completion(.failure(error: generalError))
                return
            }
            switch(serviceResponse) {
                case .success(let userData):
                    guard
                        let userData = userData,
                        let usersJSON = try? JSONDecoder().decode([UserJSON].self, from: userData),
                        let user = strongSelf.createUserAdapter(userJSON: usersJSON.first)
                    else {
                        completion(.failure(error: generalError))
                        return
                    }
                    completion(.success(response: user))
                case .failure(let error):
                    completion(.failure(error: error))
            }
        }
    }
}

private extension GetUserAdapter {
    func createUserAdapter(userJSON: UserJSON?) -> UserAdapter? {
        guard let userJSON = userJSON else {
            return nil
        }
        let geo = GeoAdapter(lat: userJSON.address.geo.lat, lng: userJSON.address.geo.lng)
        
        let address = AddressAdapter(street: userJSON.address.street,
                                     suite: userJSON.address.suite,
                                     city: userJSON.address.city,
                                     zipcode: userJSON.address.zipcode,
                                     geo: geo)
        
        let company = CompanyAdapter(name: userJSON.company.name,
                                     catchPhrase: userJSON.company.catchPhrase,
                                     bs: userJSON.company.bs)
        
        return UserAdapter(id: userJSON.id,
                           name: userJSON.name,
                           username: userJSON.username,
                           email: userJSON.email,
                           address: address,
                           phone: userJSON.phone,
                           website: userJSON.website,
                           company: company)
    }
}

private struct UserJSON: Decodable {
    var id: Int
    var name: String
    var username: String
    var email: String
    var address: Address
    var phone: String
    var website: String
    var company: Company
    
    struct Address: Decodable {
        var street: String
        var suite: String
        var city: String
        var zipcode: String
        var geo: Geo
    }
    
    struct Geo: Decodable {
        var lat: String
        var lng: String
    }
    
    struct Company: Decodable {
        var name: String
        var catchPhrase: String
        var bs: String
    }
}

private struct UserAdapter: User {
    var id: Int
    var name: String
    var username: String
    var email: String
    var address: Address
    var phone: String
    var website: String
    var company: Company
}

private struct AddressAdapter: Address {
    var street: String
    var suite: String
    var city: String
    var zipcode: String
    var geo: Geo
}

private struct CompanyAdapter: Company {
    var name: String
    var catchPhrase: String
    var bs: String
}

private struct GeoAdapter: Geo {
    var lat: String
    var lng: String
}
