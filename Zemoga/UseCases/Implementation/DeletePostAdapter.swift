//
//  DeletePostAdapter.swift
//  Zemoga
//
//  Created by Jonathand Alberto Serrano Serrano on 3/13/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

final class DeletePostAdapter: DeletePost {
    
    struct Dependencies {
        var persistence: Persistence = ServiceLocator.inject()
    }
    
    let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func execute(id: Int, completion: PersistencePostCompletion) {
        dependencies.persistence.deletePost(id: id, completion: completion)
    }
}
