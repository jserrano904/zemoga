//
//  GetFavoritesAdapter.swift
//  Zemoga
//
//  Created by Jonathand Alberto Serrano Serrano on 3/13/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

final class GetFavoritesAdapter: GetFavorites {
    
    struct Dependencies {
        var persistence: Persistence = ServiceLocator.inject()
    }
    
    let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func execute(completion: @escaping PostsCompletion) {
        let posts = dependencies.persistence.fetchPostFavorites()
        guard !posts.isEmpty else {
            completion(.success(response: []))
            return
        }
        completion(.success(response: posts))
    }
}
