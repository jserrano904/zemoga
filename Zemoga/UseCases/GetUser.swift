//
//  GetUser.swift
//  Zemoga
//
//  Created by Jonathand Alberto Serrano Serrano on 3/12/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

enum UserResponse {
    case success(response: User)
    case failure(error: Error)
}

typealias UserCompletion = (UserResponse) -> Void

protocol GetUser {
    func execute(userId: Int, completion: @escaping UserCompletion)
}
