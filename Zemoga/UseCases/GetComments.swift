//
//  GetComments.swift
//  Zemoga
//
//  Created by Jonathand Alberto Serrano Serrano on 3/12/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

enum CommentsResponse {
    case success(comments: [Comment])
    case failure(error: Error)
}

typealias CommentsCompletion = (CommentsResponse) -> Void

protocol GetComments {
    func execute(postId: Int, completion: @escaping CommentsCompletion)
}
