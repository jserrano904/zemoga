//
//  DeleteAllPosts.swift
//  Zemoga
//
//  Created by Jonathand Alberto Serrano Serrano on 3/12/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

protocol DeleteAllPosts {
    func execute(completion: PersistencePostCompletion)
}
