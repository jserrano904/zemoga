//
//  UserViewModel.swift
//  Zemoga
//
//  Created by Jonathand Alberto Serrano Serrano on 3/13/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

struct UserViewModel {
    var name: String
    var email: String
    var phone: String
    var website: String
}
