//
//  PostViewModel.swift
//  Zemoga
//
//  Created by jonathand alberto serrano serrano on 3/12/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

enum PostState: String {
    case read
    case unread
}

struct PostViewModel {
    var title: String
    var indicatorImage: String
    var id: Int
    var favorite: Bool
    var body: String
    var userId: Int
}
