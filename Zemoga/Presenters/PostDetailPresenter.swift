//
//  PostDetailPresenter.swift
//  Zemoga
//
//  Created by Jonathand Alberto Serrano Serrano on 3/12/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

protocol PostDetailPresenterProtocol {
    func format(_ user: User)
    func format(_ comments: [Comment])
    func format(_ description: String)
}

final class PostDetailPresenter: PostDetailPresenterProtocol {
    
    weak var view: PostDetailViewController?
    
    func format(_ user: User) {
        let userViewModel = UserViewModel(
            name: user.name,
            email: user.email,
            phone: user.phone,
            website: user.website)
        
        view?.update(userViewModel: userViewModel)
    }
    
    func format(_ comments: [Comment]) {
        let commentsViewModel = comments.map { formatComment(comment: $0) }
        view?.update(comments: commentsViewModel)
    }
    
    func format(_ description: String) {
        view?.updateDescription(content: description)
    }
}
private extension PostDetailPresenter {
    func formatComment(comment: Comment) -> CommentViewModel {
        return CommentViewModel(body: comment.body)
    }
}
