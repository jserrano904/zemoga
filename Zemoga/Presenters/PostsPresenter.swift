//
//  PostsPresenter.swift
//  Zemoga
//
//  Created by jonathand alberto serrano serrano on 3/10/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation
import UIKit

protocol PostsPresenterProtocol {
    func format(_ posts: [Post])
}

final class PostsPresenter: PostsPresenterProtocol {
    
    struct Constants {
        static let unreadImage = "blueIndicator-icon"
        static let FavoriteImage = "favorite-icon"
        static let limitToShowIndicator: Int = 20
    }
    
    weak var view: PostsViewController?
    
    func format(_ posts: [Post]) {
        let viewModels = posts.map{ formatPost(post: $0, index: $0.id) }.sorted(by: { $0.id < $1.id })
        view?.update(viewModels: viewModels)
    }
}

private extension PostsPresenter {
    func formatPost(post: Post, index: Int) -> PostViewModel {
        let indicator: String
        if post.status == .unread {
            indicator = index <= Constants.limitToShowIndicator ? Constants.unreadImage : ""
        } else if post.favorite {
            indicator = Constants.FavoriteImage
        } else {
            indicator = ""
        }
        return PostViewModel(title: post.title,
                             indicatorImage: indicator,
                             id: index,
                             favorite: post.favorite,
                             body: post.body,
                             userId: post.userId)
    }
}
