//
//  CommentTableViewCell.swift
//  Zemoga
//
//  Created by Jonathand Alberto Serrano Serrano on 3/13/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var commentLabel: UILabel!
    
    static let identifier: String = "CommentTableViewCell"

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        commentLabel.text = nil        
    }
    
    func config(commentViewModel: CommentViewModel) {
        commentLabel.text = commentViewModel.body
    }
}
