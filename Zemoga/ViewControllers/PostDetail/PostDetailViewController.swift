//
//  PostDetailViewController.swift
//  Zemoga
//
//  Created by Jonathand Alberto Serrano Serrano on 3/12/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import UIKit

protocol PostDetailViewControllerProtocol {
    func update(userViewModel: UserViewModel)
    func update(comments: [CommentViewModel])
    func updateDescription(content: String)
}

class PostDetailViewController: UIViewController {
    
    @IBOutlet private weak var favoriteButtonItem: UIBarButtonItem!
    @IBOutlet private weak var descriptionContentLabel: UILabel!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var emailLabel: UILabel!
    @IBOutlet private weak var phoneLabel: UILabel!
    @IBOutlet private weak var websiteLabel: UILabel!
    @IBOutlet private weak var tableView: UITableView!
    
    var interactor: PostDetailInteractor?
    var post: PostViewModel?
    private var commentsViewModel: [CommentViewModel] = []
    private var userViewModel: UserViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        interactor?.handleSceneLoaded(post: post)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        interactor?.handleSceneWillAppear()
    }
    
    @IBAction func didTapFavorite(_ sender: Any) {
        guard let postViewModel = post else { return }
        
        let imageFav = postViewModel.favorite ? "favitem-icon" : "favorite-selected-icon"
        favoriteButtonItem.image = UIImage(named: imageFav)
        
        interactor?.handleFavoriteSelection(id: postViewModel.id, isFavorite: !postViewModel.favorite)
    }
    @IBAction func didTapBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

private extension PostDetailViewController {
    func setup() {
        configUI()
        let interactor = PostDetailInteractor(dependencies: PostDetailInteractor.Dependencies())
        let presenter = PostDetailPresenter()
        presenter.view = self
        interactor.presenter = presenter
        self.interactor = interactor
        configTableView()
        validateFavoriteImage()
    }
    
    func validateFavoriteImage() {
        guard let postViewModel = post else { return }
        let imageFav = postViewModel.favorite ? "favorite-selected-icon" : "favitem-icon"
        favoriteButtonItem.image = UIImage(named: imageFav)
    }
    
    func configTableView() {
        registerCells()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    func registerCells() {
        guard let tableView = tableView else {
            return
        }
        tableView.register(
            UINib.init(nibName: CommentTableViewCell.identifier, bundle: nil),
            forCellReuseIdentifier: CommentTableViewCell.identifier)
        
        tableView.register(
            UINib.init(nibName: HeaderTableViewHeaderFooter.identifier, bundle: nil),
            forHeaderFooterViewReuseIdentifier: HeaderTableViewHeaderFooter.identifier)
    }
    
    func configUI() {
        navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

extension PostDetailViewController: PostDetailViewControllerProtocol {
    func update(userViewModel: UserViewModel) {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.nameLabel.text = userViewModel.name
            strongSelf.emailLabel.text = userViewModel.email
            strongSelf.phoneLabel.text = userViewModel.phone
            strongSelf.websiteLabel.text = userViewModel.website
        }
    }
    
    func update(comments: [CommentViewModel]) {
        self.commentsViewModel = comments
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    func updateDescription(content: String) {
         DispatchQueue.main.async { [weak self] in
            self?.descriptionContentLabel.text = content
        }
    }
}

extension PostDetailViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentsViewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = commentsViewModel[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: CommentTableViewCell.identifier, for: indexPath) as! CommentTableViewCell
        cell.config(commentViewModel: viewModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: HeaderTableViewHeaderFooter.identifier) as! HeaderTableViewHeaderFooter
        return header
    }
    
}
