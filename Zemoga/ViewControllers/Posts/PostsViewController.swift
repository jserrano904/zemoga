//
//  PostsViewController.swift
//  Zemoga
//
//  Created by jonathand alberto serrano serrano on 3/10/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import UIKit

protocol PostsViewControllerProtocol {
    func update(viewModels: [PostViewModel])
}

class PostsViewController: UIViewController {
    
    struct Constants {
        static let zemogaColor: UIColor = UIColor(red: 0.04, green: 0.68, blue: 0.05, alpha: 1)
        static let seguePostsFromPostDetail: String = "segueFromPostsToPostDetail"
    }
    
    var interactor: PostsInteractor?
    var viewModels: [PostViewModel] = []
    private var postSelected: Int = 0
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet weak var segment: UISegmentedControl!
    
    @IBAction func didTapSegment(_ sender: Any) {
        let selected = segment.selectedSegmentIndex
        interactor?.handleSegmentSelected(index: selected)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        interactor?.handleSceneLoaded()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor?.handleSceneWillAppear()
    }
    
    @IBAction func refreshButtonItem(_ sender: Any) {
        interactor?.handleRefreshButtonItemSelection()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard
            let identifier = segue.identifier,
            identifier == Constants.seguePostsFromPostDetail,
            let viewController = segue.destination as? PostDetailViewController
        else {
            return
        }
        
        let post = viewModels[postSelected]
        viewController.post = post
    }
}

private extension PostsViewController {
    func setup() {
        self.configUI()
        let interactor = PostsInteractor(dependencies: PostsInteractor.Dependencies())
        let presenter = PostsPresenter()
        presenter.view = self
        interactor.presenter = presenter
        self.interactor = interactor
        configTableView()        
    }
    func configTableView() {
        registerCells()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    func configUI() {
        UIApplication.shared.statusBarView?.tintColor = Constants.zemogaColor
        navigationController?.navigationBar.barTintColor = Constants.zemogaColor
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
    }
    
    func registerCells() {
        guard let tableView = tableView else {
            return
        }
        tableView.register(
            UINib.init(nibName: PostTableViewCell.identifier, bundle: nil),
            forCellReuseIdentifier: PostTableViewCell.identifier)
        
        tableView.register(
            UINib.init(nibName: DeleteAllPostsTableViewCell.identifier, bundle: nil),
            forHeaderFooterViewReuseIdentifier: DeleteAllPostsTableViewCell.identifier)
    }
}

extension PostsViewController: PostsViewControllerProtocol {
    func update(viewModels: [PostViewModel]) {
        self.viewModels = viewModels
        DispatchQueue.main.async { [weak self] in            
            self?.tableView.reloadData()
        }
    }
}

extension PostsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = viewModels[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: PostTableViewCell.identifier, for: indexPath) as! PostTableViewCell
        cell.config(viewModel: viewModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        postSelected = indexPath.row
        self.performSegue(withIdentifier: Constants.seguePostsFromPostDetail, sender: self)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: DeleteAllPostsTableViewCell.identifier) as! DeleteAllPostsTableViewCell
        footer.delegate = self
        return footer
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            interactor?.handleDeletePost(indexPath: indexPath)
            viewModels.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}

extension PostsViewController: DeleteAllPostTableViewCellDelegate {
    func didTapDeleteAllPosts() {
        interactor?.handleFooterSelection()
    }
}
