//
//  DeleteAllPostsTableViewCell.swift
//  Zemoga
//
//  Created by jonathand alberto serrano serrano on 3/12/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import UIKit

protocol DeleteAllPostTableViewCellDelegate: class {
    func didTapDeleteAllPosts()
}

class DeleteAllPostsTableViewCell: UITableViewHeaderFooterView {

    static let identifier = "DeleteAllPostsTableViewCell"
    
    weak var delegate: DeleteAllPostTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()        
    }
    @IBAction func didTapDeleteAllPosts(_ sender: Any) {
        delegate?.didTapDeleteAllPosts()
    }
}
