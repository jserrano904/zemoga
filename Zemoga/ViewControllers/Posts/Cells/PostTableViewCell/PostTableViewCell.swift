//
//  PostTableViewCell.swift
//  Zemoga
//
//  Created by jonathand alberto serrano serrano on 3/12/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {

    @IBOutlet weak var titlePostLabel: UILabel!
    @IBOutlet weak var readIndicator: UIImageView!
    
    static let identifier: String = "PostTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        titlePostLabel.text = nil
        readIndicator.image = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func config(viewModel: PostViewModel){
        titlePostLabel.text = viewModel.title
        guard !viewModel.indicatorImage.isEmpty else {
            return
        }
        
        readIndicator.image = UIImage(named: viewModel.indicatorImage)
    }
}
