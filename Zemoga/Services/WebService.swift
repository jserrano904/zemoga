//
//  WebService.swift
//  Zemoga
//
//  Created by jonathand alberto serrano serrano on 3/10/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

typealias SuccessResponse = Data?

enum ServiceReponse {
    case success(response: SuccessResponse)
    case failure(error: Error)
}

typealias WebServiceResponse = (ServiceReponse) -> Void

protocol WebService {
    func consumeEndpoint(endPoint: String, completion: @escaping WebServiceResponse)
}
