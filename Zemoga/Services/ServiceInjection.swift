//
//  ServiceInjection.swift
//  Zemoga
//
//  Created by jonathand alberto serrano serrano on 3/10/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

final class ServiceLocator {
    
    static let shared: ServiceLocator = ServiceLocator()
    
    var webService: WebService {
        return WebServiceImp()
    }
    
    var persistence: Persistence {
        return PersistenceImp()
    }
}

extension ServiceLocator {
    static private var injector: ServiceLocator {
        return ServiceLocator.shared
    }
    
    static func inject() -> WebService {
        return injector.webService
    }
    
    static func inject() ->Persistence {
        return injector.persistence
    }
}
