//
//  Persistence.swift
//  Zemoga
//
//  Created by Jonathand Alberto Serrano Serrano on 3/12/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

typealias PersistencePostCompletion = (() -> Void)?

protocol Persistence {
    func savePosts(posts: [Post])
    func fetchPosts() -> [Post]
    func fetchPostFavorites() -> [Post]
    func deleteAllPosts(completion: PersistencePostCompletion)
    func deletePost(id: Int, completion: PersistencePostCompletion)
    func updatePost(_ postId: Int, isFavorite: Bool, status: PostState, completion: PersistencePostCompletion)
}
