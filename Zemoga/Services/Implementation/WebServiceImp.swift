//
//  WebServiceImp.swift
//  Zemoga
//
//  Created by jonathand alberto serrano serrano on 3/10/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

final class WebServiceImp: WebService {
    
    func consumeEndpoint(endPoint: String, completion: @escaping WebServiceResponse) {
        guard let url = URL(string: endPoint) else { return }
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            let generalError = NSError()
            guard let error = error else {
                guard
                    let httpResponse = response as? HTTPURLResponse,
                    httpResponse.statusCode == 200,
                    let data = data
                else {
                    completion(.failure(error: generalError))
                    return
                }
                
                //Success
                completion(.success(response: data))
                return
            }
            
            completion(.failure(error: error))
        }
        
        dataTask.resume()
    }
}
