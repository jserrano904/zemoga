//
//  PersistenceImp.swift
//  Zemoga
//
//  Created by Jonathand Alberto Serrano Serrano on 3/12/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation
import UIKit
import CoreData

private final class ZemogaPersistentContainer: NSPersistentContainer {
    override class func defaultDirectoryURL() -> URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    }
}

final class PersistenceImp: Persistence {
    private struct DBConstants {
        static let database = "ZemogaModel"
    }
    
    private lazy var persistentContainer: ZemogaPersistentContainer = {
        let container = ZemogaPersistentContainer(name: DBConstants.database)
        let directoryURL = ZemogaPersistentContainer.defaultDirectoryURL()
        container.loadPersistentStores { _, error in
            if let error = error {
                fatalError("Unable to load managed object model erorr: \(error)")
            }
        }
        return container
    }()
    
    func savePosts(posts: [Post]) {
        persistentContainer.viewContext.perform {
            posts.forEach { [weak self] in
                self?.createPostPersistenceObject(post: $0)
            }
            try? self.persistentContainer.viewContext.save()
        }
    }
    
    func fetchPosts() -> [Post] {
        let storedMessages: [Post] = fetchAllPosts()
        return storedMessages
    }
    
    func deleteAllPosts(completion: PersistencePostCompletion) {
        let fetchRequest = fetchRequestForEntity(PostManagedObject.self)
        persistentContainer.viewContext.perform {
            self.deleteObjectsIn(fetchRequest: fetchRequest, context: self.persistentContainer.viewContext)
            try? self.persistentContainer.viewContext.save()
            completion?()
        }
    }
    
    func deletePost(id: Int, completion: PersistencePostCompletion) {
        let predicate = NSPredicate(format: "attrId = %i", id)
        let fetchRequest = fetchRequestForEntity(PostManagedObject.self)
        fetchRequest.predicate = predicate
        persistentContainer.viewContext.perform {
            self.deleteObjectsIn(fetchRequest: fetchRequest, context: self.persistentContainer.viewContext)
            try? self.persistentContainer.viewContext.save()
            completion?()
        }
    }
    
    func updatePost(_ postId: Int, isFavorite: Bool, status: PostState, completion: PersistencePostCompletion) {
        let predicate = NSPredicate(format: "attrId = %i", postId)
        let request = fetchRequestForEntity(PostManagedObject.self)
        request.predicate = predicate        
        persistentContainer.viewContext.perform {
            let postsManagedObject = (try? self.persistentContainer.viewContext.fetch(request)) ?? []
            
            guard let postManagedObject = postsManagedObject.first else {
                return
            }
            
            postManagedObject.attrFavorite = isFavorite
            postManagedObject.attrStatus = status.rawValue
            
            try? self.persistentContainer.viewContext.save()
            completion?()
        }
    }
    
    func fetchPostFavorites() -> [Post] {
        let storedMessages: [Post] = fetchFavoritesPost()
        return storedMessages
    }
}

private extension PersistenceImp {
    func createPostPersistenceObject(post: Post){
        let postManagedObject = PostManagedObject(context: persistentContainer.viewContext)
        postManagedObject.attrUserId = Int16(post.userId)
        postManagedObject.attrId = Int16(post.id)
        postManagedObject.attrBody = post.body
        postManagedObject.attrTitle = post.title
        postManagedObject.attrFavorite = post.favorite
        postManagedObject.attrStatus = post.status.rawValue
    }
    
    func fetchAllPosts() -> [PostManagedObject] {
        let request = fetchRequestForEntity(PostManagedObject.self)
        let sort = NSSortDescriptor(key: #keyPath(PostManagedObject.attrId), ascending: true)
        request.sortDescriptors = [sort]
        return (try? persistentContainer.viewContext.fetch(request)) ?? []
    }
    
    func fetchFavoritesPost() -> [PostManagedObject] {
        let predicate = NSPredicate(format: "attrFavorite = %@", NSNumber(value: true))
        let request = fetchRequestForEntity(PostManagedObject.self)
        request.predicate = predicate
        let sort = NSSortDescriptor(key: #keyPath(PostManagedObject.attrId), ascending: true)
        request.sortDescriptors = [sort]
        return (try? persistentContainer.viewContext.fetch(request)) ?? []
    }
        
    func fetchRequestForEntity<T: NSManagedObject>(_ entityClass: T.Type) -> NSFetchRequest<T> {
        return NSFetchRequest<T>(entityName: String(describing: entityClass))
    }
    
    func deleteObjectsIn<T: NSManagedObject>(fetchRequest: NSFetchRequest<T>, context: NSManagedObjectContext) {
        fetchRequest.includesPropertyValues = false
        let items = (try? context.fetch(fetchRequest)) ?? []
        items.forEach(context.delete(_:))
    }
}
