//
//  Post.swift
//  Zemoga
//
//  Created by jonathand alberto serrano serrano on 3/11/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

protocol Geo {
    var lat: String { get }
    var lng: String { get }
}

protocol Address {
    var street: String { get }
    var suite: String { get }
    var city: String { get }
    var zipcode: String { get }
    var geo: Geo { get }    
}

protocol Company {
    var name: String { get }
    var catchPhrase: String { get }
    var bs: String { get }
}

protocol User {
    var id: Int { get }
    var name: String { get }
    var username: String { get }
    var email: String { get }
    var address: Address { get }
    var phone: String { get }
    var website: String { get }
    var company: Company { get }
}

protocol Post {
    var userId: Int { get }
    var id: Int { get }
    var title: String { get }
    var body: String { get }
    var favorite: Bool { get }
    var status: PostState { get }
}

protocol Comment {
    var id: Int { get }
    var name: String { get }
    var email: String { get }
    var body: String  { get }    
}
