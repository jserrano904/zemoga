//
//  PostManagedObject.swift
//  Zemoga
//
//  Created by Jonathand Alberto Serrano Serrano on 3/12/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation
import CoreData

class PostManagedObject: NSManagedObject {
    @NSManaged public var attrUserId: Int16
    @NSManaged public var attrId: Int16
    @NSManaged public var attrTitle: String
    @NSManaged public var attrBody: String
    @NSManaged public var attrStatus: String
    @NSManaged public var attrFavorite: Bool
}

extension PostManagedObject: Post {
    var favorite: Bool {
        return attrFavorite
    }
    
    var status: PostState {
        return attrStatus == "read" ? .read : .unread
    }
    
    var userId: Int {
        return Int(attrUserId)
    }
    
    var id: Int {
        return Int(attrId)
    }
    
    var title: String {
        return attrTitle
    }
    
    var body: String {
        return attrBody
    }
}
