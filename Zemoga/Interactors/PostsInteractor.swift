//
//  PostsInteractor.swift
//  Zemoga
//
//  Created by jonathand alberto serrano serrano on 3/10/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

protocol PostsInteractorProtocol {
    func handleSceneWillAppear()
    func handleSceneLoaded()
    func handleFooterSelection()
    func handleRefreshButtonItemSelection()
    func handlePostSelection(indexPath: IndexPath) -> Post?
    func handleSegmentSelected(index: Int)
    func handleDeletePost(indexPath: IndexPath)
}

protocol PostsDataSource {
    var postID: Int? { get }
}

final class PostsInteractor: PostsInteractorProtocol {
    
    struct Dependencies {
        var getPosts: GetPosts = UseCaseLocator.inject()
        var deleteAllPosts: DeleteAllPosts = UseCaseLocator.inject()
        var getFavorites: GetFavorites = UseCaseLocator.inject()
        var deletepost: DeletePost = UseCaseLocator.inject()
    }
    
    var presenter: PostsPresenterProtocol?
    var dependencies: Dependencies
    private var posts: [Post] = []
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func handleSceneWillAppear() {
        refreshPosts(usingCache: true)
    }
    
    func handleSceneLoaded() {
        
    }
    
    func handleFooterSelection() {
        dependencies.deleteAllPosts.execute { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.presenter?.format([])
            strongSelf.posts = []
        }
    }
    
    func handleRefreshButtonItemSelection() {
        dependencies.deleteAllPosts.execute { [weak self] in
            self?.refreshPosts(usingCache: false)
        }
    }
    
    func handlePostSelection(indexPath: IndexPath) -> Post? {
        return posts[indexPath.row]
    }
    
    func handleSegmentSelected(index: Int){
        switch index {
            case 0:
                refreshPosts(usingCache: true)
            case 1:
                favoritePosts()
            default:
                break
        }
    }
    
    func handleDeletePost(indexPath: IndexPath) {
        dependencies.deletepost.execute(id: indexPath.row) { [weak self] in
            self?.refreshPosts(usingCache: true)
        }
    }
}

private extension PostsInteractor {
    func refreshPosts(usingCache: Bool) {
        dependencies.getPosts.execute(usingCache: usingCache, completion: { [weak self] (postResponse) in
            guard let strongSelf = self else { return }
            switch(postResponse) {
            case .success(let posts):
                strongSelf.presenter?.format(posts)
                strongSelf.posts = posts
            case .failure(let error):
                print("error: \(error)")
            }
        })
    }
    func favoritePosts() {
        dependencies.getFavorites.execute { [weak self] (postResponse) in
            guard let strongSelf = self else { return }
            switch(postResponse) {
            case .success(let posts):
                strongSelf.presenter?.format(posts)
                strongSelf.posts = posts
            case .failure(let error):
                print("error: \(error)")
            }
        }
    }
}
