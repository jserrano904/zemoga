//
//  PostDetailInteractor.swift
//  Zemoga
//
//  Created by Jonathand Alberto Serrano Serrano on 3/12/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import Foundation

protocol PostDetailInteractorProtocol {
    func handleSceneWillAppear()
    func handleSceneLoaded(post: PostViewModel?)
    func handleFavoriteSelection(id: Int?, isFavorite: Bool)
}

final class PostDetailInteractor: PostDetailInteractorProtocol {
    struct Dependencies {
        var getUser: GetUser = UseCaseLocator.inject()
        var getComments: GetComments = UseCaseLocator.inject()
        var updatePost: UpdatePost = UseCaseLocator.inject()
        var getPosts: GetPosts = UseCaseLocator.inject()
    }
    
    var presenter: PostDetailPresenterProtocol?
    var dependencies: Dependencies
    private var post: PostViewModel?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func handleSceneWillAppear() {
        refreshDetail()
    }
    
    func handleSceneLoaded(post: PostViewModel?) {
        self.post = post
        guard let post = post else { return }
        presenter?.format(post.body)
        dependencies.updatePost.execute(postId: post.id, isFavorite: post.favorite, status: .read) {}
    }
    
    func handleFavoriteSelection(id: Int?, isFavorite: Bool) {
        guard let id = id else { return }
        
        dependencies.updatePost.execute(postId: id, isFavorite: true, status: .read) { [weak self] in
            self?.dependencies.getPosts.execute(usingCache: true, completion: { _ in })
        }
    }
}

private extension PostDetailInteractor {
    func refreshDetail() {
        guard
            let userId = post?.userId,
            let postId = post?.id
        else {
            return
        }
        
        dependencies.getUser.execute(userId: userId) { [weak self] (userResponse) in
            switch(userResponse) {
                case .success(let user):
                    self?.presenter?.format(user)
                case .failure(let error):
                    print("error: \(error.localizedDescription)")
            }
        }
        
        dependencies.getComments.execute(postId: postId) { [weak self] (commentsResponse) in
            switch(commentsResponse) {
            case .success(let comments):
                self?.presenter?.format(comments)
            case .failure(let error):
                print("error: \(error.localizedDescription)")
            }
        }
    }
}
