//
//  UIApplication.swift
//  Zemoga
//
//  Created by Jonathand Alberto Serrano Serrano on 3/12/19.
//  Copyright © 2019 jonathand alberto serrano serrano. All rights reserved.
//

import UIKit

extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}
